# Conversion Tracker: Voluum Integration

You can now integrate the Content.ad Conversion tracker with [Voluum](https://voluum.com/), a popular tool to manage online campaigns. We recommend Voluum for those partners looking to manage your online marketing campaigns across several platforms, including Content.ad. It is one of the leading platforms.

### TL;DR

-   Step 1: Navigate to "Add Traffic Source Postback URL" in Voluum
-   Step 2: Add `{clickid}` to your Offer URL
-   Step 3: Type "ck" and `[click_key]` into the Voluum "External ID" fields
-   Step 4: Paste your Content.ad Conversion Tracker URL into the "Traffic source postback URL" field


    https://px.content-ad.net/widget/conversion-s2s?affiliateGuid=######&clickKey={externalid}&user={uid}&conversionType=checkout&amount={payout}&productSku={offer.id}&customerId={uid}&invoiceId={transaction.id}

-   Setp 5: Done! Conversions should be captured and tracked in near real time. Please allow 5-10 minutes for the first conversions to come through.

### A little more detail:

**Step 1: Navigate to "Add Traffic Source Postback URL" in Voluum**

Login to your Voluum account and follow the instructions from Voluum on how to include a "[Traffic Source Postback URL](https://doc.voluum.com/en/traffic_source_postback_url.html)" in their documentation.

**Step 2: Add "{clickid}" to your Offer URL**

Navigate to 'Offers' and click edit. Add `{clickid}` as a parameter at the end of your offer URL.

![Voluum Traffic Source Postback URL Parameters](../../assets/images/voluum2.png)

**Step 3: Type "ck" and "[click_key]" into the Voluum "External ID" fields, as displayed below.**

![Voluum Traffic Source Postback URL Parameters](../../assets/images/voluum1.png)

**Step 4: Paste your Content.ad Conversion Tracker URL into the "Traffic source postback URL" field**

To find your Account GUID: Login to app.content.ad. In the main menu, select the "General Info" page. At the top of the page, find your `aid` (Account GUID) value.

![Go to app.content.ad/MyAccount > "Account GUID"](../../assets/images/contentad1.png)

Then, copy the below URL into a text editor like notepad. Replace the `######` with your Account GUID from the General Info page above. 

    https://px.content-ad.net/widget/conversion-s2s?affiliateGuid=######&clickKey={externalid}&user={uid}&conversionType=checkout&amount={payout}&productSku={offer.id}&customerId={uid}&invoiceId={transaction.id}

Return to Voluum, and paste your Conversion Tracker URL into the "Traffic Source Postback URL" field.

**Step 5: Done**

Conversions should be captured and tracked in near real time. Please allow 5-10 minutes for the first conversions to come through.
