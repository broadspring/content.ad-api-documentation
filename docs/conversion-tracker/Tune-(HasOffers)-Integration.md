# Conversion Tracker: Tune (HasOffers) Integration

You can now integrate the Content.ad Conversion tracker with [Tune](https://www.tune.com/) (previously known as HasOffers), a tool to manage online campaigns. We recommend Tune for those partners looking to manage your online marketing campaigns across several platforms, including Content.ad.

### TL;DR

-   Step 1: Add a new Conversion Pixel/URL in Tune
-   Step 2: Select "Publisher: Content.ad", and "Type: Postback Pixel"
-   Step 3: Paste your Content.ad Conversion Tracker URL into the "Conversion Pixel Code / URL" field


    https://px.content-ad.net/widget/conversion-s2s?affiliateGuid=#############&clickKey={aff_click_id}&user={aff_unique1}&conversionType=checkout&amount={payout}&productSku={offer_id}&customerId={aff_unique2}&invoiceId={transaction_id}

-   Step 4: Create a custom mapping between Tune and Content.ad
-   Setp 5: Done! Conversions should be captured and tracked in near real time. Please allow 5-10 minutes for the first conversions to come through.

### A little more detail:

**Step 1: Add a new Conversion Pixel/URL in Tune**

Login to your Tune account and select "Publisher" from the left menu, then select "Content.ad". Click the "Add" button in the "Conversion Pixel / URLs" section.

![tune.com > Publisher > Content.ad](../../assets/images/tune1.png)

**Step 2: Select "Publisher: Content.ad", and "Type: Postback Pixel"**

* Enter your "Offer" and "Goal" information
* Select "Content.ad" from the "Publisher" drop-down
* Select "Postback Pixel" from the "Type" drop-down

![Enter Conversion Pixel / URL Parameters](../../assets/images/tune2.png)

**Step 3: Paste your Content.ad Conversion Tracker URL into the "Conversion Pixel Code / URL" field**

To find your Account GUID: Login to app.content.ad. In the main menu, select the "General Info" page. At the top of the page, find your `aid` (Account GUID) value.

![Go to app.content.ad/MyAccount > "Account GUID"](../../assets/images/contentad1.png)

Then, copy the below URL into a text editor like notepad. Replace the `######` with your Account GUID from the General Info page above.

    https://px.content-ad.net/widget/conversion-s2s?affiliateGuid=#############&clickKey={aff_click_id}&user={aff_unique1}&conversionType=checkout&amount={payout}&productSku={offer_id}&customerId={aff_unique2}&invoiceId={transaction_id}

Return to Tune, and paste your Conversion Tracker URL into the "Conversion Pixel Code / URL" field.

**Step 4: Create a custom mapping between Tune and Content.ad**

Click on "Add Source", "Add Sub IDs”", and "Add Click ID”" in the Tune Tracking link section.
* Add Content.ad macros as shown in the image below and click on update in each section.

![Tune Tracking Link section setup](../../assets/images/tune3.png)

* After creating mappings in Tune, your tracking link URL will look like the following sample URL. Use your tracking link URL in the Content.ad while creating the ads.

> Sample Tune Tracking Link URL
> 
> https://tracking.examples.com/aff_c?offer_id=#####&aff_id=####&aff_sub=[cid]&aff_sub2=[did]&aff_click_id=[ck]&aff_unique1=[uid]&aff_unique2=[euid]

**Step 5: Done**

Conversions should be captured and tracked in near real time. Please allow 5-10 minutes for the first conversions to come through.